# GitlabPinger

GitlabPinger provides a simple way to check the response times of
several Gitlab related websites.

## Installation

To install the CLI tool you have to install the gem:

    $ gem install gitlab_pinger

## Usage

Once installed, the following commands can be invoked

```
Commands:
  gitlab_pinger about_gitlab_com  # Pings about.gitlab.com for 1 minute in 10 second intervals
  gitlab_pinger gitlab_com        # Pings gitlab.com for 1 minute in 10 second intervals
  gitlab_pinger help [COMMAND]    # Describe available commands or one specific command
```

Both methods `about_gitlab_com` and `gitlab_com` support a silent
option. When invoked with `-s` or `--silent` parameter, there will be no command line output except error messages.

The output will look like this:

```
gitlab.com
Number of tries: 6
Number of successes: 5
Number of failures: 1
Average response time: 0.67912 s.
```

The following HTTP response codes are considered success:

* 200 OK
* 302 Found

## Development

### Contributing

Thank you for your interest in contributing to this project. Your efforts are highly appreciated.
While contributing, please follow the [code of conduct](CODE_OF_CONDUCT.md), so that everyone can be included.
Your code will be covered by the [project license](LICENSE.md).

### Development

#### Setup

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

#### Proceedings

In order to contribute, please
* Fork this project repository.
* Choose an issue to work on.
  * **Existing issue**: please make sure to comment and verify no one else is working on the issue already.
  * **New feature/issue**: please create an issue first.
* Add the feature or fix the bug you’ve chosen to work on.
* Open a merge request. The earlier you open a merge request, the sooner you can get feedback. You can mark it as a Work in Progress to signal that you’re not done yet.
* Add tests and documentation if needed, as well as a changelog entry.
* Make sure the test suite is passing.
* Wait for a reviewer. You’ll likely need to change some things once the reviewer has completed a code review for your merge request. You may also need multiple reviews depending on the size of the change.
* Get your changes merged!

#### Changelog

When or when not to add to the changelog:

* Any user-facing change should have a changelog entry. Example: "gitlab-pinger now uses terminal output in colors."
* A fix for a regression introduced and then fixed in the same release (i.e., fixing a bug introduced during a monthly release candidate) should not have a changelog entry.
* Any API changes (especially breaking changes) should have a changelog entry. Example: "Method Pinger#ping\_once added. It pings the given URL just once."
* Any internal change (e.g., refactoring, technical debt remediation, test suite changes) should not have a changelog entry. Example: "Reduce spec startup time."
* Any contribution from a community member, no matter how small, may have a changelog entry regardless of these guidelines if the contributor wants one. Example: "Fixed a typo on the search results page. (Jane Smith)"
* Performance improvements should have a changelog entry.

### Versioning

This gem uses [semantic versioning](https://semver.org/spec/v2.0.0.html).

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).


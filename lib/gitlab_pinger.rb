# frozen_string_literal: true

require 'gitlab_pinger/version'
require 'ping_result'
require 'net/http'
require 'benchmark'
require 'rest-client'

module GitlabPinger
  ##
  # This class provides methods to ping specific URLs for
  # #duration minutes every #interval seconds.
  # The methods are expected to return a hash.
  #
  class Ping
    attr_reader :duration, :interval

    # Initializer
    #
    # @param duration [Number] sites will be pinged for X minutes
    # @param interval [Number] sites will be pinged every X seconds
    def initialize(duration: 1, interval: 10)
      @duration = duration
      @interval = interval
    end

    # @param url [String] the URL that should be probed
    # @return [Hash] the ping result for the given URL in
    #   the form { tries: x, success: y, failed: z, average_response_time: a}
    def ping(url)
      ping_request(url)
    end

    private

    ##
    # Sends HTTP GET requests to the given URL.
    # It does this for #duration minutes in #interval seconds.
    # @param url [String] address to ping
    # @return [Hash] of the form { tries: 3, success: 2, failed: 1 }
    #
    def ping_request(url)
      result = PingResult.new
      finish_time = current_time + duration * 60
      while current_time <= finish_time
        worked, time_rounded = timed_ping(url)
        worked ? result.ping_success(time_rounded) : result.ping_failed
        sleep interval
      end
      result.to_h
    end

    ##
    # Measures the time a ping attempt took
    # @param url [String] address to ping
    # @return [Bool, Number] time spent and wether the attempt was successful
    def timed_ping(url)
      worked = nil
      time_elapsed = Benchmark.realtime { worked = pingable?(url) }
      time_rounded = time_elapsed.round(5)
      [worked, time_rounded]
    end

    ##
    # Issues a HTTP GET request.
    # Returns true for successful requests.
    # The following response codes are considered success:
    #  * 200 OK
    #  * 302 Found (Moved temporarily)
    # @param url [String] location to ping
    # @return [Bool] true if url could be pinged, false for failures.
    def pingable?(url)
      res = RestClient::Request.execute(
        method: :get, url: url, max_redirects: 0
      )
      res.code.to_i == 200
    # RestClient considers HTTP response 302 worth an exception
    rescue RestClient::Found
      true
    rescue RestClient::ExceptionWithResponse => e
      STDERR.puts "The following error occured while pinging '#{url}':"
      STDERR.puts e
      false
    end

    ##
    # @return [Number] value of the monotonic system clock (aka 'uptime')
    # see https://blog.dnsimple.com/2018/03/elapsed-time-with-ruby-the-right-way/
    def current_time
      Process.clock_gettime(Process::CLOCK_MONOTONIC)
    end
  end
end

# frozen_string_literal: true

##
# Encapsulates the result for a pinging run.
class PingResult
  attr_reader :tries, :success, :failed

  def initialize
    @tries = 0
    @success = 0
    @failed = 0
    @measured_times = []
  end

  # Adds a successful ping to the counter
  # @param time_elapsed [Number] number of seconds the ping took
  def ping_success(time_elapsed)
    @tries += 1
    @success += 1
    @measured_times << time_elapsed
  end

  # Adds a failed ping to the counter
  def ping_failed
    @tries += 1
    @failed += 1
  end

  # @return [Hash] condensed result
  def to_h
    {
      tries: @tries,
      success: @success,
      failed: @failed,
      average_response_time: average_response_time
    }
  end

  private

  def average_response_time
    return nil if @measured_times.empty?
    sum_times = @measured_times.inject { |sum, el| sum + el }.to_f
    (sum_times / @measured_times.size).round(5)
  end
end

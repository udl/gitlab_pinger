# Current development

## Features

* Output of CLI tasks shows number of tries, number of success and
  failures and average response time
* Add silent option to CLI
* Added CLI gitlab\_pinger thor task as executable
* Added Pinger#ping method that pings arbitrary URLs

## Bugfixes


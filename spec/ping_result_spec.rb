# frozen_string_literal: true

require 'spec_helper'
require 'ping_result'

describe PingResult do
  %i[tries success failed].each do |field|
    it "has a '#{field}' value" do
      expect(subject).to respond_to field
    end
  end

  context 'on success' do
    it 'increments the success counter' do
      expect { subject.ping_success(0.1) }.to change { subject.success }.by 1
    end

    it 'increments the tries counter' do
      expect { subject.ping_success(0.1) }.to change { subject.tries }.by 1
    end

    it 'doesn\'t increment the failed counter' do
      expect { subject.ping_success(0.1) }.not_to change { subject.failed }
    end
  end

  context 'on failure' do
    it 'increments the failed counter' do
      expect { subject.ping_failed }.to change { subject.failed }.by 1
    end

    it 'increments the tries counter' do
      expect { subject.ping_failed }.to change { subject.tries }.by 1
    end

    it 'doesn\'t increment the success counter' do
      expect { subject.ping_failed }.not_to change { subject.success }
    end
  end

  context 'average_response_time' do
    it 'returns nil if no time has been recorded' do
      expect(subject.tries).to eq 0
      expect(subject.to_h[:average_response_time]).to eq nil
    end

    it 'returns the average time' do
      subject.ping_success(0.1)
      subject.ping_success(0.3)
      expect(subject.to_h[:average_response_time]).to eq 0.2
    end

    it 'doesn\'t take failed pings into account for average response time' do
      subject.ping_success(0.1)
      subject.ping_success(0.3)
      subject.ping_failed
      expect(subject.to_h[:average_response_time]).to eq 0.2
    end
  end
end

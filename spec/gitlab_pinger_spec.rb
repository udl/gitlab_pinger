# frozen_string_literal: true

require 'spec_helper'

describe GitlabPinger do
  it 'has a version number' do
    expect(GitlabPinger::VERSION).not_to be nil
  end

  context GitlabPinger::Ping do
    subject { GitlabPinger::Ping.new }

    it 'has a method to ping URLs' do
      expect(subject).to respond_to :ping
    end
  end

  # SHARED EXAMPLES
  shared_examples_for 'pinging website is successful' do
    it 'returns a hash' do
      expect(result).to be_a Hash
    end

    it 'returns a hash where all keys are symbols' do
      expect(result.keys.all? { |k| k.class == Symbol }).to be_truthy
    end

    %i[tries success failed average_response_time].each do |field|
      it "returns a hash containing the key '#{field}'" do
        expect(result.keys).to include(field)
      end
    end

    it 'returns several tries' do
      expect(result[:tries]).to be > 0
    end

    it 'returns success for all gitlab.com tries' do
      expect(result[:tries]).to eq(result[:success])
    end

    it 'returns a positive value for average response time' do
      expect(result[:average_response_time]).to be > 0
    end
  end

  shared_examples_for 'pinging website failed' do
    it 'doesn\'t raise' do
      expect { result }.not_to raise_error
    end

    it 'returns some failures' do
      expect(result[:failed]).to be > 0
    end

    it 'returns several tries' do
      expect(result[:tries]).to be > 0
    end

    it 'doesn\'t take failed pings into account for average response time' do
      expect(result[:average_response_time]).to be_nil
    end
  end

  # ACTUAL SPECS
  context 'redirecting URLs' do
    let(:url) { 'https://gitlab.com' }

    # shorter duration and interval to make test run faster
    subject(:result) do
      GitlabPinger::Ping.new(duration: 0.01, interval: 0.3).ping(url)
    end

    context 'when all goes well' do
      before do
        stub_request(:get, url).to_return(
          body: '<html><body>redirect</body></html>',
          status: 302
        )
      end
      it_should_behave_like 'pinging website is successful'
    end
    context 'with internal server error' do
      before do
        stub_request(:get, url).to_return(
          body: 'Internal server error',
          status: 500
        )
      end
      it_should_behave_like 'pinging website failed'
    end
    context 'when request times out' do
      before do
        stub_request(:get, url).to_timeout
      end
      it_should_behave_like 'pinging website failed'
    end
  end
  context 'responding URLs' do
    let(:url) { 'https://about.gitlab.com' }

    # shorter duration and interval to make test run faster
    subject(:result) do
      GitlabPinger::Ping.new(duration: 0.01, interval: 0.3).ping(url)
    end

    context 'when all goes well' do
      before do
        stub_request(:get, url).to_return(
          body: '<html><body><h1>gitlab</h1></body></html>',
          status: 200
        )
      end
      it_should_behave_like 'pinging website is successful'
    end
    context 'with internal server error' do
      before do
        stub_request(:get, url).to_return(
          body: 'Internal server error',
          status: 500
        )
      end
      it_should_behave_like 'pinging website failed'
    end
    context 'when request times out' do
      before do
        stub_request(:get, url).to_timeout
      end
      it_should_behave_like 'pinging website failed'
    end
  end
end

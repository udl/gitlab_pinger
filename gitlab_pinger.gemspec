# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gitlab_pinger/version'

Gem::Specification.new do |spec|
  spec.name          = 'gitlab_pinger'
  spec.version       = GitlabPinger::VERSION
  spec.authors       = ['Udo Groebner']
  spec.email         = ['udo@groebnerweb.de']

  spec.summary       = 'CLI tool that checks the status of several gitlab URLs'
  spec.homepage      = 'https://gitlab.com/udl/gitlab_pinger'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'TODO Set to your server'
  else
    raise 'RubyGems 2.0 or newer is required to protect against public'\
          ' gem pushes.'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'bin'
  spec.executables   = 'gitlab_pinger'
  spec.require_paths = ['lib']

  spec.add_dependency 'bundler', '~> 1.10'
  spec.add_dependency 'rest-client'
  spec.add_dependency 'thor'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'webmock'
end
